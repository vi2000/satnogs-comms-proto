# SatNOGS COMMS Protobuf messaging specification
This repository contains the Protobuf messaging specification used by
the [reference design](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu) of the SatNOGS-COMMS board.